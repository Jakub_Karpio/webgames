# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
# vendor/assets/javascripts directory can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file. JavaScript code in this file should be added after the last require_* statement.
#
# Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
# about supported directives.
#
#= require jquery
#= require jquery_ujs
#= require jquery-ui
#= require bootstrap
#= require turbolinks

#= require_tree .
#= require framework/application
#= require_tree ./framework/views
#= require_tree ./framework/widgets


$(document).on 'ready page:load', ->
  className = $('body').attr('data-js-class-name')
  window.applicationView = try
    eval("new #{className}()")
  catch error
    new Views.ApplicationView()
  window.applicationView.render()

$(window).unload ->
  window.applicationView.cleanup()


$(document).on 'ready page:load', ->
  $(document).on 'click', '.btn-registration', (event) ->
    if $('#sign_up').hasClass('model-open')
    else
      $('#sign_up').addClass('show model-open').removeClass('hide fade in').css({'display':'block'})
      $('.modal-backdrop').css({'position':'relative'})
  $(document).on 'click', '.btn-edit', (event) ->
    if $('#edit_up').hasClass('model-open')
    else
      $('#edit_up').addClass('show model-open').removeClass('hide fade in').css({'display':'block'})
      $('.modal-backdrop').css({'position':'relative'})
  $(document).on 'click', '.btn-login', (event) ->
    if $('#sign_in').hasClass('model-open')
    else
      $('#sign_in').addClass('show model-open').removeClass('hide fade in').css({'display':'block'})
      $('.modal-backdrop').css({'position':'relative'})
  $(document).on 'click', '.btn-password', (event) ->
    if $('#forgot_password').hasClass('model-open')
    else
      $('#forgot_password').addClass('show model-open').removeClass('hide fade in').css({'display':'block'})
      $('.modal-backdrop').css({'position':'relative'})
