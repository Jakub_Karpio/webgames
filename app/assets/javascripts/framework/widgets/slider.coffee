prepare 'Widgets'

class Widgets.Slider

  @enable: (params) ->
    $('.main-slider').slick(
      params
    )

  @cleanup: ->
    $('.main-slider').slick('unslick')
