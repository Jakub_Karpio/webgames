window.prepare = (path, target = window) ->
  arr =  path.split('.')
  target = target[arr.shift()] ||= {}

  return true if arr.length == 0
  prepare arr.join('.'), target

window.Views ||= {}

class Views.ApplicationView
  render: ->

  cleanup: ->
