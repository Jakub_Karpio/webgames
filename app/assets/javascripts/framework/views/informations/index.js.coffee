prepare 'Views.Informations'

class Views.Informations.Index extends Views.ApplicationView
  render: ->
        $ ->
          $('.row-info').on
            click: ->
              if $('.news-list').find('.box-img').hasClass('yellowing')
                $('.box-img').removeClass 'yellowing'
                $(this).find('.box-img').addClass('yellowing')
              else
                $(this).find('.box-img').addClass('yellowing')
        $ ->
          if $('.news-list').find('.box-img').hasClass('yellowing')
            $('.box-img').removeClass('yellowing')
          else
             $('.news-list').find('.box-img').first().addClass('yellowing')

    $(document).on 'turbolinks:load', ->
      url = document.URL
      informationId = url.split('_id=')[1]
      boxImg = document.getElementById(informationId)

      boxImg.classList.add('yellowing')
      $('.row-info').on
        click: ->
          if $('.news-list').find('.box-img').hasClass('yellowing')
            $('.box-img').removeClass 'yellowing'
            $(this).find('.box-img').addClass('yellowing')
          else
            $(this).find('.box-img').addClass('yellowing')
