prepare 'Views.Home'

class Views.Home.Index extends Views.ApplicationView
  render: ->
    super()
    Widgets.Slider.enable({
      dots: true,
      arrows: false,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      centerMode: true,
      autoplay: true,
      autoplaySpeed: 3500,
      variableWidth: true
    })


  cleanup: ->
    super()
    Widgets.Slider.cleanup()
