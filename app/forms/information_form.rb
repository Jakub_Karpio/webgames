require 'reform'

class InformationForm < Reform::Form
  include ActiveAdmin::Reform::ActiveRecord

  model :information

  property :title, validates: { presence: true }
  property :short_information, validates: { presence: true, length: { maximum:200 } }
  property :long_information
  property :game_id
  property :information_image, populate_if_empty: InformationImage, validates: { presence: true } do
    property :image
  end

end
