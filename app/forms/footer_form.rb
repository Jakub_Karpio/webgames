require 'reform'

class FooterForm < Reform::Form
  include ActiveAdmin::Reform::ActiveRecord

  model :footer

  property :content, validates: { presence: true, length: { maximum:15 } }

end
