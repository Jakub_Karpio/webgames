require 'reform'

class GameForm < Reform::Form
  include ActiveAdmin::Reform::ActiveRecord

  model :game

  property :title, validates: { presence: true, length: { maximum:15 } }
  property :link, validates: { presence: true }
  property :published
  property :image, populate_if_empty: Image, validates: { presence: true } do
    property :image
  end

end
