require 'reform'

class ContactForm < Reform::Form
  include ActiveAdmin::Reform::ActiveRecord

  model :contact

  property :title, validates: { presence: true }
  property :content, validates: { presence: true }
  property :sender, validates: { presence: true, format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i } }
  
end
