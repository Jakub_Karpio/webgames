require 'reform'

class UserForm < Reform::Form
  include ActiveAdmin::Reform::ActiveRecord

  model :user

  property :first_name
  property :last_name
  property :email, validates: { presence: true, format: { with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i } }
  property :nick, validates: { presence: true }
  property :password
  property :password_confirmation
  property :avatar

end
