class Contacts::Create
  ValidationError = Class.new(StandardError)

  def initialize(contact_form)
    @contact_form = contact_form
  end

  def process
    raise ValidationError if contact_form.invalid?
    ActiveRecord::Base.transaction do
      contact = Contact.create!(contact_form.contact_params)
    end
  end

  private

  attr_reader :contact_form

end
