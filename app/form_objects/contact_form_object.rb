class ContactFormObject < FormObject
  include ActiveModel::Model
  CONTACT_ATTRS = %i[content title sender email]

  attr_accessor(*CONTACT_ATTRS)

  validates :content, :title, :sender, :email, presence: true
  validates :email, format: { with: EMAIL_VALIDATION, message: 'musi spełniać wymogi.
    Przykładowo: email@gmail.com' }

  def contact_params
    object_attrs_from_fields(CONTACT_ATTRS)
  end
end
