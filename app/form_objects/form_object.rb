class FormObject
  include ActiveModel::Model

  EMAIL_VALIDATION = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  def initialize(args = {})
    args.each do |k, v|
      instance_variable_set("@#{k}", v) unless v.nil?
    end
  end

  def persisted?
    @id.present?
  end

  def object_attrs_from_fields(object_fields)
    attrs = {}
    object_fields.each do |f|
      attrs[f] = instance_variable_get("@#{f}")
    end
    attrs
  end
end
