class HomeController < ApplicationController
  helper_method :newest_information_id

  def index
    render locals: {
      newest_informations: Information.order("created_at DESC").first(4).compact,
      newest_games: Game.published.order("created_at DESC").first(6).compact,
      slider_images: SliderImage.all
    }
  end

  private

  def newest_information_id(game_id)
    return unless newest_information(game_id).present?
    newest_information(game_id)
  end

  def newest_information(game_id)
    Information.where(game_id: game_id).order("created_at DESC").first
  end

end
