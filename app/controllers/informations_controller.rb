class InformationsController < ApplicationController
  def index
    render locals: {
      informations: Information.order('created_at DESC'),
      information: active_information
    }
  end

  def show
    render locals: {
      information: Information.find(params[:id])
    }
  end

  def new
  end

  def create
  end

  def update
  end

  def edit
  end

  def destroy
  end

  private
  def information_params
    params.require(:information).permit(:title, :short_information, :long_information)
  end

  def active_information
     params.has_key?(:information_id) ? Information.find(params[:information_id]) : Information.last
  end

end
