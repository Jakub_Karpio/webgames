class GamesController < ApplicationController
  before_action :logged_user_only, only: :show
  before_action :check_if_public, only: :show
  helper_method :newest_information_id

  def index
    render locals: {
      games: Game.published.order("created_at DESC").paginate(page: params[:page], per_page: 6)
    }
  end

  def show
    render locals: {
      game: Game.find(params[:id])
    }
  end

  private
  
  def game_params
    params.require(:game).permit(:title, :link)
  end

  def logged_user_only
    redirect_to root_path unless current_user.present?
  end

  def check_if_public
    redirect_to games_path unless Game.find(params[:id]).published?
  end

  def newest_information_id(game_id)
    return unless newest_information(game_id).present?
    newest_information(game_id)
  end

  def newest_information(game_id)
    Information.where(game_id: game_id).order("created_at DESC").first
  end

end
