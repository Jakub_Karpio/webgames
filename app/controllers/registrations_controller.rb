class RegistrationsController < Devise::RegistrationsController

  def new
    build_resource
    yield resource if block_given?

    respond_with resource
  end

  def create
    build_resource(sign_up_params)

    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :alert, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :alert, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      if User.exists?(email: resource.email)
        if User.exists?(nick: resource.nick)
          set_flash_message! :alert, :email_and_nick_exists
          redirect_to root_path
        else
          set_flash_message! :alert, :email_exists
          redirect_to root_path
        end
      else
        if User.exists?(nick: resource.nick)
          set_flash_message! :alert, :nick_exists
          redirect_to root_path
        else
          if !resource.password.match(/^(?=.*?[0-9])(?=.*[A-Z]).{6,}$/)
            set_flash_message! :alert, :wrong_password
            redirect_to root_path
          else
            set_flash_message! :alert, :wrong_registration
            redirect_to root_path
          end
        end
      end
    end
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)
    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ? :update_needs_confirmation : :updated
        set_flash_message :alert, flash_key
      end
      bypass_sign_in resource, scope: resource_name
      respond_with resource, location: after_update_path_for(resource)
    else
      if !resource.email.present?
        set_flash_message! :alert, :wrong_mail
        redirect_to root_path
      end
      if User.exists?(nick: resource.nick)
        set_flash_message! :alert, :wrong_nick
        redirect_to root_path
      else
        redirect_to informations_path
      end
    end
  end

  protected

  def update_resource(resource, account_update_params)
    resource.update_without_password(account_update_params)
  end

  private

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :nick, :avatar, :email, :password, :password_confirmation)
  end

end
