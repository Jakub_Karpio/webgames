class ContactsController < ApplicationController

    def index
    end

    def show
      render :show, locals: { contact: contact }
    end

    def new
      render :new, locals: { contact_form: ContactFormObject.new() }
    end

    def create
      contact_form = ContactFormObject.new(contact_params)
      begin
        Contacts::Create.new(contact_form).process
        render :info
      rescue Contacts::Create::ValidationError
        render :new, locals: { contact_form: contact_form }
      end
    end

    def destroy
      if contact.destroy
        flash[:notice] = 'Wykasowano wiadomość'
      else
        flash[:alert] = 'Nie udało się skasować wiadomości'
      end
    end

    private

    def contact_params
      params.require(:contact).permit(:content, :title, :sender, :email)
    end

    def contact
      @contact ||= Contact.find(params[:id])
    end
end
