ActiveAdmin.register Footer do
  permit_params :content
  form_class FooterForm

  form do |f|
    f.inputs "Add footer" do
      f.input :content, required: true
    end
    f.actions
  end

end
