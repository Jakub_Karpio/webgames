ActiveAdmin.register Game do
  permit_params :image, :title, :link, :published, :image_id, image_attributes: [:id,:image]
  form_class GameForm

  index do
     selectable_column
     id_column
     column 'Title', :title
     column 'Link', :link
     column 'Published', :published
     column 'Image', sortable: :image_file_name do |game|
       link_to game.image.image_file_name, game.image.image.url
     end
     column :created_at
     actions
  end

  form do |f|
    f.inputs "Add game" do
      f.input :title, required: true
      f.input :link, required: true
      f.input :published, required: true
      f.inputs for: [:image, f.object.image || Image.new] do |s|
        s.input :image, label: "Image size: 301x170", as: :file, required: true
      end
    end

    f.actions
  end
end
