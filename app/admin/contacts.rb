ActiveAdmin.register Contact do
  permit_params :title, :content, :sender
  form_class ContactForm

  index do
    selectable_column
    id_column
    column 'Title', :title
    column 'Message', :content
    column 'Sender', :sender
    column 'Email', :email
    column 'Send at', :created_at
    actions
  end

  form do |f|
    f.inputs "Create message" do
      f.input :title
      f.input :content, required: true
      f.input :sender, required: true
    end

    f.actions
  end
end
