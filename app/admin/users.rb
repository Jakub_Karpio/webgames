ActiveAdmin.register User do
  permit_params :first_name, :last_name, :email, :nick, :avatar, :password, :password_confirmation
  form_class UserForm

  index do
      selectable_column
      id_column
      column 'First name', :first_name
      column 'Last name', :last_name
      column 'Email', :email
      column 'Nick', :nick
      column 'Avatar', :avatar
      column 'hash_pass', :encrypted_password
      column :created_at
      actions
  end

  form do |f|
    f.inputs "Add User" do
      f.input :first_name
      f.input :last_name
      f.input :email, required: true
      f.input :nick, required: true
      f.input :avatar
      if f.object.new_record?
        f.input :password
        f.input :password_confirmation
      end
    end
    f.actions
  end

end
