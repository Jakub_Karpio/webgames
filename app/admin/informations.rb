ActiveAdmin.register Information do
  permit_params :title, :short_information, :long_information, :game_id, :information_image, information_image_attributes: [:id,:image]
  form_class InformationForm

  index do
    selectable_column
    id_column
    column 'Title', :title
    column 'short_information', :short_information
    column 'long_information', :long_information do |information|
      controller.html_truncate(information.long_information, 100).html_safe
    end
    column 'Game', sortable: :game_title do |information|
      information.game
    end
    column 'Image', sortable: :image_file_name do |information|
      link_to information.information_image&.image_file_name, information.information_image.image.url
    end
    column :created_at
    actions
  end

  controller do
    def html_truncate(html, truncate_length)
      Admin::Informations::HtmlTruncate.new(html, truncate_length).process
    end
  end


  show do
    attributes_table do
      row 'id', :id
      row 'short_information', :short_information
      row 'long_information', :long_information do |information|
        information.long_information.html_safe
      end
      row 'Game', :game_title do |information|
        information.game
      end
      row 'Image', :image_file_name do |information|
        link_to information.information_image&.image_file_name, information.information_image.image.url
      end
      row :created_at
    end
  end

  form do |f|
    f.inputs "Add information" do
      f.input :title, required: true
      f.input :short_information, required: true
      f.input :long_information, as: :trumbowyg, required: true
      f.input :game_id, :as => :select, :collection => Game.all, include_blank: false
      f.inputs for: [:information_image, f.object.information_image || InformationImage.new] do |s|
        s.input :image, label: "Image size: 240x166", :as => :file, required: true
      end
    end

    f.actions
  end

end
