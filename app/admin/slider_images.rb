ActiveAdmin.register SliderImage do
  permit_params :image

  index do
    id_column
    column :image_file_name
    column 'Image', sortable: :image_file_name do |image|
      link_to image.image_file_name, image.image.url
    end
    column :created_at
    actions
  end


  form do |f|
    f.inputs "Add slider image" do
      f.input :image, label: "Image size: 1921x613", as: :file, required: true
    end
    f.actions
  end

end
