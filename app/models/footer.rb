class Footer < ApplicationRecord
  validate :check_record, on: :create

  def check_record
   if Footer.all.count === 1
     errors.add(:content, "Footer exists")
   end
  end
end
