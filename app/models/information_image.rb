class InformationImage < ApplicationRecord
  belongs_to :information, dependent: :destroy

  has_attached_file :image
  do_not_validate_attachment_file_type :image
end
