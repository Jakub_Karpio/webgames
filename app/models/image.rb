class Image < ApplicationRecord
  belongs_to :game, dependent: :destroy

  has_attached_file :image
  do_not_validate_attachment_file_type :image
end
