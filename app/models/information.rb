class Information < ApplicationRecord
  has_one :information_image
  belongs_to :game
end
