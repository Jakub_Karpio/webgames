class Game < ApplicationRecord
  has_one :image
  has_many :informations
  default_scope { order('created_at DESC') }

  scope :published, -> { where(published: true ) }
end
