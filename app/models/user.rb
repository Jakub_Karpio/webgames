class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
       :recoverable, :rememberable, :validatable, :confirmable
  validates :nick, uniqueness: true, presence: true
  validate :password_complexity
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, :default_url => "/images/missingavatar.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  def password_complexity
    if ( password.present? && !password.match(/^(?=.*?[0-9])(?=.*[A-Z]).{6,}$/))
      errors.add :password, "Password must contain at least one number and one uppercase letter"
    end
  end

  def send_confirmation_instructions
    unless @raw_confirmation_token
      generate_confirmation_token!
    end
    opts = pending_reconfirmation? ? { to: email } : { }
    send_devise_notification(:confirmation_instructions, @raw_confirmation_token, opts)
  end

  def update_without_password(params, *options)
    result = update(params, *options)
    result
  end
end
