# encoding: UTF-8
require_relative '../acceptance_helper'

feature 'Contacts', js: true do
  scenario 'should be able to send contact request without loging in' do
    expect(User.count).to eq 0
    expect(Contact.count).to eq 0
    visit root_path
    click_on 'KONTAKT'
    within '#modal-custom' do
      fill_in 'Imię lub nick', with: 'Max'
      fill_in 'E-mail', with: 'maxix@xxx.pl'
      fill_in 'Temat', with: 'I would like more money'
      find('#contact_content').set('Give me more money please. I would like that a lot. Thank you')
      find('.send').click
    end
    expect(page).to have_content 'Prośba o kontakt wysłana'
    expect(Contact.count).to eq 1
  end

  context 'Need to fill in all fields' do
    before :each do
      visit root_path
      click_on 'KONTAKT'
    end

    scenario 'content' do
      within '#modal-custom' do
        fill_in 'Imię lub nick', with: 'Max'
        fill_in 'E-mail', with: 'maxix@xxx.pl'
        fill_in 'Temat', with: 'I would like more money'
        find('.send').click
      end
      expect(page).to have_content 'Treść wiadomości nie może być pusta'
      expect(page).to_not have_content 'Prośba o kontakt wysłana'
      expect(Contact.count).to eq 0
    end
    scenario 'email' do
      within '#modal-custom' do
        fill_in 'Imię lub nick', with: 'Max'
        fill_in 'Temat', with: 'I would like more money'
        find('#contact_content').set('Give me more money please. I would like that a lot. Thank you')
        find('.send').click
      end
      expect(page).to have_content 'E-mail nie może być pusty'
      expect(page).to_not have_content 'Prośba o kontakt wysłana'
      expect(Contact.count).to eq 0
    end
    scenario 'name, nick' do
      within '#modal-custom' do
        fill_in 'E-mail', with: 'maxix@xxx.pl'
        fill_in 'Temat', with: 'I would like more money'
        find('#contact_content').set('Give me more money please. I would like that a lot. Thank you')
        find('.send').click
      end
      expect(page).to have_content 'Imię lub nick nie może być puste'
      expect(page).to_not have_content 'Prośba o kontakt wysłana'
      expect(Contact.count).to eq 0
    end
    scenario 'subject' do
      within '#modal-custom' do
        fill_in 'Imię lub nick', with: 'Max'
        fill_in 'E-mail', with: 'maxix@xxx.pl'
        find('#contact_content').set('Give me more money please. I would like that a lot. Thank you')
        find('.send').click
      end
      expect(page).to have_content 'Temat nie może być pusty'
      expect(page).to_not have_content 'Prośba o kontakt wysłana'
      expect(Contact.count).to eq 0
    end
  end
end
