#encoding: utf-8
require File.expand_path(File.dirname(__FILE__) + "/../rails_helper")
#Dir.glob(Rails.root.join('spec', 'acceptance', 'shared', '**', '*.rb'), &method(:require))
require 'capybara-webkit'
require 'capybara-screenshot'
require 'capybara-screenshot/rspec'

Capybara.javascript_driver = :webkit

Capybara.default_selector = :css
Capybara.server_port = 54544 + ENV['TEST_ENV_NUMBER'].to_i
Capybara.asset_host = 'http://localhost:3000'
Capybara::Screenshot.webkit_options = { width: 1920, height: 1080 }
Capybara.default_max_wait_time = 10
Capybara.save_path = Rails.root.join('tmp','capybara').to_s

Capybara::Webkit.configure do |config|
  config.allow_url("fonts.googleapis.com")
  config.allow_url("http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css")
  config.allow_url("netdna.bootstrapcdn.com")
  config.allow_url("https://cdn.linearicons.com/free/1.0.0/icon-font.min.css")
  config.allow_url("cdn.linearicons.com")
  config.block_unknown_urls
end
include ActionDispatch::TestProcess

FileUtils.rm(Dir.glob(Rails.root.join('tmp/capybara/*'))) #this line removes all screenshots before running test

def sign_in_as(user = create(:user))
  visit "/autologin?id=#{user.id}"
  user
end

def browser_console_logs
  page.driver.console_messages.map do |message|
    p <<-LOG
      Line number: #{message[:line_number]}
      Message: #{message[:message]}
    LOG
  end
end
