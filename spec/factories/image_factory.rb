# encoding: utf-8
FactoryBot.define do
 factory :image do
  image { File.open("#{Rails.root}/spec/assets/thumbnail.png") }
 end
end
