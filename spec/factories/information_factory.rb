# encoding: utf-8
FactoryBot.define do
 factory :information do
  title
  short_information
  long_information
  game { build :game }
  information_image { build :information_image }
 end
end
