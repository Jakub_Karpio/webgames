# encoding: utf-8
FactoryBot.define do
 factory :game do
  title
  link
  published true
  image { build :image }
 end
end
