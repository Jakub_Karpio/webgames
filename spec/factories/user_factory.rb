# encoding: utf-8
FactoryBot.define do
  factory :user do
    first_name 'Jazz'
    last_name 'Jackrabbit'
    email 'email@pl.pl'
    nick 'JJ'
    password 'Password1'
    password_confirmation 'Password1'
  end
end
