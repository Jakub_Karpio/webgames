# encoding: utf-8
FactoryBot.define do
 factory :information_image do
  image { File.open("#{Rails.root}/spec/assets/thumbnail.png") }
 end
end
