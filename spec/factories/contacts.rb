FactoryBot.define do
  factory :contact do
    title "MyString"
    content "MyString"
    sender "MyString"
  end
end
