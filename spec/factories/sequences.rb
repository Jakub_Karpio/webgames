# encoding: utf-8
FactoryBot.define do
  sequence(:short_information) do |n|
    "short_information #{n}"
  end
  sequence(:long_information) do |n|
    "long_information #{n}"
  end
  sequence(:title) do |n|
    "title #{n}"
  end
  sequence(:link) do |n|
    "link#{n}"
  end
  sequence(:first_name) do |n|
    "first_name #{n}"
  end
  sequence(:last_name) do |n|
    "last_name #{n}"
  end
  sequence(:email) do |n|
    "mail#{n}@mail.mail"
  end
  sequence(:nick) do |n|
    "nick #{n}"
  end
end
