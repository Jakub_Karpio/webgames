require 'simplecov'
SimpleCov.start 'rails' do
  add_filter '/bin/'
  add_filter '/db/'
  add_filter '/spec/' # for rspec
  # filtering out controllers and models cause it woudl basically be testing rails features. Instead follow
  # the form object -> service flow and write specs for those
  add_filter '/controllers'
  add_filter '/models'
end

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'
require 'capybara/rails'
require 'spec_helper'
ActiveRecord::Migration.maintain_test_schema!

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
  include ActionDispatch::TestProcess
  config.include Formulaic::Dsl
  config.include FactoryBot::Syntax::Methods
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!
  config.formatter = :documentation
  config.before(:suite) do
    DatabaseCleaner.strategy = :deletion
    DatabaseCleaner.clean_with(:deletion)
  end

  config.before(:each) do
    ActionMailer::Base.deliveries = []
    FactoryBot.reload
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

  config.verbose_retry = true
  config.display_try_failure_messages = true
  Shoulda::Matchers.configure do |configure|
    configure.integrate do |with|
      with.test_framework :rspec
      with.library :rails
    end
  end
end
