require 'rails_helper'

describe Contacts::Create do

  subject { described_class.new(contact_form).process }

  describe '#process' do
    let(:contact_form) { ContactFormObject.new(contact_params) }

    context 'valid' do
      let(:contact_params) do
        {
          sender: 'Ja',
          email: 'kkk@pp.pl',
          title: 'Pieniadze',
          content: 'Daj'
        }
      end
        it { subject; expect(Contact.last.valid?).to eq true }
        it { expect{ subject }.to change{ Contact.count }.by(1) }
    end

    context 'invalid' do
      let(:contact_params) do
        {
          sender: 'On',
          email: 'ppp@pl.as',
          title: '',
          content: 'Po prostu daj'
        }
      end
      it { expect{subject}.to raise_error(described_class::ValidationError) }
    end
  end
end
