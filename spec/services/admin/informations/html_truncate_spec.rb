require 'rails_helper'

describe Admin::Informations::HtmlTruncate do

  subject { described_class.new(html,truncate_length).process }

  describe '#process' do
    let(:html) { "<p>sl</p><p><strong>slowo</strong></p><p>s</p>" }

    context 'truncated' do
      let(:truncate_length) { 3 }
      it { subject; expect(subject).to eq "<p>sl</p><p><strong>slowo" }
    end

    context 'nottruncated' do
      let(:truncate_length) { 9 }
      it { subject; expect(subject).to eq "<p>sl</p><p><strong>slowo</strong></p><p>s</p>" }
    end

  end
end
