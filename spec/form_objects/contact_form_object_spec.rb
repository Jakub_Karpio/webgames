# frozen_string_literal: true
require 'rails_helper'
describe ContactFormObject do
  subject { described_class.new(attributes) }

  let(:base_attributes) { { sender: 'GracZ', title: 'granie', email: 'gracz@pl.pl', content: 'gram w gry' } }

  context 'valid' do
    let(:attributes) { base_attributes }
    it 'is valid with all data present and email in correct form' do
      expect(subject).to be_valid
    end
  end

  context 'invalid with email incorrect' do
    let(:attributes) { base_attributes.merge!(email: 'meh') }
    it do
      expect(subject).to_not be_valid
    end
  end
  context 'invalid with email blank' do
    let(:attributes) { base_attributes.merge!(email: nil) }
    it do
      expect(subject).to_not be_valid
    end
  end
  context 'invalid with content blank' do
    let(:attributes) { base_attributes.merge!(content: nil) }
    it do
      expect(subject).to_not be_valid
    end
  end
  context 'invalid with sender blank' do
    let(:attributes) { base_attributes.merge!(sender: nil) }
    it do
      expect(subject).to_not be_valid
    end
  end
  context 'invalid with title blank' do
    let(:attributes) { base_attributes.merge!(title: nil) }
    it do
      expect(subject).to_not be_valid
    end
  end
end
