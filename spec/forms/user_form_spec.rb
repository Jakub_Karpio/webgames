require 'rails_helper'

describe UserForm do
  subject { described_class.new(user) }
  describe 'user form validations' do
    context 'all fields present' do
      let(:user) { create :user }
      it 'valid' do
        expect(subject).to be_valid
      end
    end

    context 'does not require first name' do
      let(:user) { create :user, first_name: "" }
      it 'valid' do
        expect(subject).to be_valid
      end
    end

    context 'does not require last name' do
      let(:user) { create :user, last_name: "" }
      it 'valid' do
        expect(subject).to be_valid
      end
    end

    context 'requires email' do
      let(:user) { build :user, email: "" }
      it 'invalid' do
        expect(subject).to_not be_valid
        expect(subject.errors.messages[:email]).to eq(["nie może być pusty", "musi odpowiadać formatowi email@gmail.com"])
      end
    end

    context 'wrong email' do
      let(:user) { build :user, email: "zly email" }
      it 'invalid' do
        expect(subject).to_not be_valid
        expect(subject.errors.messages[:email]).to eq(["musi odpowiadać formatowi email@gmail.com"])
      end
    end

    context 'requires nick' do
      let(:user) { create :user, nick: "" }
      it 'invalid' do
        expect(subject).to_not be_valid
        expect(subject.errors.messages[:nick]).to eq(["nie może być pusty"])
      end
    end
  end
end
