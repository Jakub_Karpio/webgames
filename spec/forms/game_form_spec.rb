require 'rails_helper'

describe GameForm do
  subject { described_class.new(game) }
  describe 'game form validations' do
    context 'all fields present' do
      let(:game) { create :game }
      it 'valid' do
        expect(subject).to be_valid
      end
    end
    context 'requires title' do
      let(:game) { create :game, title: "" }
      it 'invalid' do
        expect(subject).to_not be_valid
        expect(subject.errors.messages[:title]).to eq(["nie może być pusty"])
      end
    end
    context 'requires link' do
      let(:game) { create :game, link: "" }
      it 'invalid' do
        expect(subject).to_not be_valid
        expect(subject.errors.messages[:link]).to eq(["nie może być pusty"])
      end
    end
    context 'requires image' do
      let(:game) { create :game, image: nil }
      it 'invalid' do
        expect(subject).to_not be_valid
        expect(subject.errors.messages[:image]).to eq(["nie może być pusty"])
      end
    end
  end
end
