require 'rails_helper'

describe InformationForm do
  subject { described_class.new(information) }
  describe 'information form validations' do
    context 'all fields present' do
      let(:information) {build :information}
      it 'valid' do
        expect(subject).to be_valid
      end
    end

    context 'requires title' do
      let(:information) {build :information, title: ""}
      it 'invalid' do
        expect(subject).to_not be_valid
        expect(subject.errors.messages[:title]).to eq(["nie może być puste"])
      end
    end
    context 'requires short_information' do
      let(:information) {build :information, short_information: ""}
      it 'invalid' do
        expect(subject).to_not be_valid
        expect(subject.errors.messages[:short_information]).to eq(["nie może być puste"])
      end
    end

    context 'requires image' do
      let(:information) {build :information, information_image: nil}
      it 'invalid' do
        expect(subject).to_not be_valid
        expect(subject.errors.messages[:information_image]).to eq(["nie może być puste"])
      end
    end
  end
end
