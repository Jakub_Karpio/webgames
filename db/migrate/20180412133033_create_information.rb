class CreateInformation < ActiveRecord::Migration[5.1]
  def change
    create_table :information do |t|
      t.belongs_to :game, index: true
      t.string :title
      t.string :short_information
      t.text :long_information

      t.timestamps
    end
  end
end
