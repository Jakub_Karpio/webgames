class CreateInformationImages < ActiveRecord::Migration[5.1]
  def change
    create_table :information_images do |t|
      t.integer :information_id, index: true
      t.attachment :image

      t.timestamps
    end
  end
end
