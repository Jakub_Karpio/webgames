class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :title
      t.string :content
      t.string :sender
      t.string :email

      t.timestamps
    end
  end
end
