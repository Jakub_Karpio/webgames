class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email, null: false
      t.string :nick, null: false

      t.timestamps
    end
    add_index :users, :last_name
    add_index :users, :nick
  end
end
