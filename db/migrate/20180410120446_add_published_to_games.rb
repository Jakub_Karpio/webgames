class AddPublishedToGames < ActiveRecord::Migration[5.1]
  def change
    add_column :games, :published, :boolean, default: false, null: false
  end
end
