class CreateFooters < ActiveRecord::Migration[5.1]
  def change
    create_table :footers do |t|
      t.string :content
      t.timestamps
    end
  end
end
