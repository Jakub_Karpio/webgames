source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '~> 5.1.6'
gem 'pg'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'activeadmin'
gem 'activeadmin-reform'
gem 'activeadmin_trumbowyg'
gem 'pry'
gem 'bcrypt'
gem 'factory_bot'
gem 'devise'
gem 'haml', '~> 5.0', '>= 5.0.4'
gem 'will_paginate', '~> 3.1.0'
gem 'jquery-ui-rails'
gem 'jquery-rails'
gem 'paperclip'
gem 'bootstrap', '~> 4.0'
gem 'simple_form'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'binding_of_caller'
  gem 'jazz_fingers'
  gem 'selenium-webdriver'
  gem 'rspec-rails', '~> 3.7'
  gem 'factory_bot_rails'
  # gem 'poltergeist'
  # gem 'phantomjs', require: 'phantomjs/poltergeist'
end

group :test do
  gem 'capybara'
  gem 'capybara-webkit'
  gem 'capybara-screenshot'
  gem 'database_cleaner'
  gem 'formulaic'
  gem 'rpg'
  gem 'timecop' # freeze time
  gem 'rspec-retry' # rerun failing tests
  gem 'shoulda-matchers'
  gem "json-schema"
  gem 'test-prof'
  gem 'ruby-prof'
  gem 'stackprof', '>= 0.2.9', require: false
  gem 'rblineprof'
  gem 'rblineprof-report'
  gem 'webmock'
  gem 'simplecov', :require => false
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem "capistrano", "~> 3.10", require: false
  gem "capistrano-rails", "~> 1.3", require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rvm'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
