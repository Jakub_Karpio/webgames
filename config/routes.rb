Rails.application.routes.draw do

  devise_for :users, :controllers => {
    registrations: 'registrations',
    sessions: 'sessions',
    passwords: 'passwords',
    confirmations: 'confirmations'
  }

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'home#index'
  resources :forums
  resources :games
  resources :informations
  resources :supports
  resources :contacts, except: [:update, :edit]
end
