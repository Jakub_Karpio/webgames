#role :app, %w{deploy@example.com}
#role :web, %w{deploy@example.com}
#role :db,  %w{deploy@example.com}

server 'webgames.2n.pl',
  roles:  %w(app web db),
  ssh_options: {
    user: 'webgamesuser'
  }
set :deploy_to, "/home/webgamesuser/webgames_app"
set :stage, :staging
set :branch, 'master'
