# config valid for current version and patch releases of Capistrano
lock "~> 3.10.2"

set :application, "galaktus"
set :repo_url, "ssh://git@bitbucket.org/Jakub_Karpio/webgames.git"

set :user, 'ovhuser'

set :stages, %w(staging production)
set :default_stage, "staging"
set :rvm_ruby_version, '2.5.0'

set :linked_files, %w{config/database.yml config/secrets.yml}
set :linked_dirs, %w{private public/assets public/system log tmp pids}

set :kepp_assets, 5
set :passenger_restart_with_touch, true

namespace :deploy do
  after :publishing, :restart

  task :restart do
    on roles(:all), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

end
